function Pregunta() {

  this.estadoJuego = false;
  this.totalPreguntas = 10;
  this.preguntaActual = 0;
  this.preguntas = [{
    'valpregunta': {
      'pregunta': 'Cuando te pegás en el dedo chiquito del pie con la punta de la cama ¿qué hacés?Toroooo es conan',
      'respuestas': [
        {
          'r1': 'Te hacés incapacitar por una 1 semana'
        },
        {
          'r2': 'Te tirás al piso y llorás hasta que venga mami'
        },
        {
          'r3': 'Te vengás de la cama pegándole una patada'
        },
        {
          'r4': 'La cama es la que llora'
        }
      ]
    }
  }];

  var self = this;

  this.muestraTest = function() {

    $("#contIniciaTest").fadeOut('400', function() {
      $("#contTestPreguntas").removeClass('hidden').fadeIn('400');
      self.estadoJuego = false;
      self.mostrarPregunta();
    });
  }

  this.mostrarPregunta = function() {

    self.estadoJuego = true;
    self.preguntaActual = self.preguntaActual + 1;
    $(".pregunta .txtpreg").html(self.preguntas[self.preguntaActual - 1].valpregunta.pregunta);

  }

}

var accionesPregunta = new Pregunta();


$('.btn-iniciartest').on("click", function() {
  accionesPregunta.muestraTest();
  return false;
});
