<?php

namespace PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brand Payment model
 *
 * @ORM\Table(name="brands_payments")
 * @ORM\Entity(repositoryClass="PaymentBundle\Repository\Brand_PaymentRepository")
 */
class Brand_Payment
{

    /**
     * @var int
     * 
     * @ORM\Id
     * @ORM\Column(name="id_brand", type="integer")
     */
    private $idBrand;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id_payment", type="integer")
     */
    private $idPayment;

    /**
     * @var float
     *
     * @ORM\Column(name="commission", type="float")
     */
    private $commission;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="brand_payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_brand", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $brand;
 
    /**
     * @ORM\ManyToOne(targetEntity="Payment_Method", inversedBy="brand_payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $payment_method;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set idBrand
     *
     * @param integer $idBrand
     * @return Brand_Payment
     */
    public function setIdBrand($idBrand)
    {
        $this->idBrand = $idBrand;

        return $this;
    }

    /**
     * Get idBrand
     *
     * @return integer 
     */
    public function getIdBrand()
    {
        return $this->idBrand;
    }

    /**
     * Set idPayment
     *
     * @param integer $idPayment
     * @return Brand_Payment
     */
    public function setIdPayment($idPayment)
    {
        $this->idPayment = $idPayment;

        return $this;
    }

    /**
     * Get idPayment
     *
     * @return integer 
     */
    public function getIdPayment()
    {
        return $this->idPayment;
    }

    /**
     * Set commission
     *
     * @param float $commission
     * @return Brand_Payment
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return float 
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Brand_Payment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Brand_Payment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set brand
     *
     * @param \PaymentBundle\Entity\Brand $brand
     * @return Brand_Payment
     */
    public function setBrand(\PaymentBundle\Entity\Brand $brand)
    {
        $this->brand = $brand;
        
        return $this;
    }

    /**
     * Get brand
     *
     * @return \PaymentBundle\Entity\Brand 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set payment_method
     *
     * @param \PaymentBundle\Entity\Payment_Method $paymentMethod
     * @return Brand_Payment
     */
    public function setPaymentMethod(\PaymentBundle\Entity\Payment_Method $paymentMethod)
    {
        $this->payment_method = $paymentMethod;

        return $this;
    }

    /**
     * Get payment_method
     *
     * @return \PaymentBundle\Entity\Payment_Method 
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }
}
