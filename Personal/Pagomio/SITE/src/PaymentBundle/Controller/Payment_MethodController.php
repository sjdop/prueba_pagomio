<?php

namespace PaymentBundle\Controller;

use PaymentBundle\Entity\Payment_Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Payment_method controller.
 *
 * @Route("payment_method")
 */
class Payment_MethodController extends Controller
{
    /**
     * Lists all payment_Method entities.
     *
     * @Route("/", name="payment_method_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $payment_Methods = $em->getRepository('PaymentBundle:Payment_Method')->findAll();

        return $this->render('payment_method/index.html.twig', array(
            'payment_Methods' => $payment_Methods,
        ));
    }

    /**
     * Creates a new payment_Method entity.
     *
     * @Route("/new", name="payment_method_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $payment_Method = new Payment_method();
        $form = $this->createForm('PaymentBundle\Form\Payment_MethodType', $payment_Method);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment_Method);
            $em->flush();

            return $this->redirectToRoute('payment_method_show', array('id' => $payment_Method->getId()));
        }

        return $this->render('payment_method/new.html.twig', array(
            'payment_Method' => $payment_Method,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a payment_Method entity.
     *
     * @Route("/{id}", name="payment_method_show")
     * @Method("GET")
     */
    public function showAction(Payment_Method $payment_Method)
    {
        $deleteForm = $this->createDeleteForm($payment_Method);

        return $this->render('payment_method/show.html.twig', array(
            'payment_Method' => $payment_Method,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing payment_Method entity.
     *
     * @Route("/{id}/edit", name="payment_method_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Payment_Method $payment_Method)
    {
        $deleteForm = $this->createDeleteForm($payment_Method);
        $editForm = $this->createForm('PaymentBundle\Form\Payment_MethodType', $payment_Method);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $payment_Method->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('payment_method_edit', array('id' => $payment_Method->getId()));
        }

        return $this->render('payment_method/edit.html.twig', array(
            'payment_Method' => $payment_Method,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a payment_Method entity.
     *
     * @Route("/{id}", name="payment_method_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Payment_Method $payment_Method)
    {
        $form = $this->createDeleteForm($payment_Method);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($payment_Method);
            $em->flush();
        }

        return $this->redirectToRoute('payment_method_index');
    }

    /**
     * Creates a form to delete a payment_Method entity.
     *
     * @param Payment_Method $payment_Method The payment_Method entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Payment_Method $payment_Method)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('payment_method_delete', array('id' => $payment_Method->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
