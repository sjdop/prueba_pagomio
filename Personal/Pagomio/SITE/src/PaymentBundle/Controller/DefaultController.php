<?php

namespace PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('count(payment_methods.id)');
        $qb->from('PaymentBundle:Payment_Method', 'payment_methods');

        $methods = $qb->getQuery()->getSingleScalarResult();

        $qb->select('count(brands.id)');
        $qb->from('PaymentBundle:Brand', 'brands');
        $brands = $qb->getQuery()->getSingleScalarResult();
        
        $user = $this->getUser();

        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'methods' => $methods,
            'brands' => $brands,
            'user' => $user
        ));
        //return $this->render('PaymentBundle::login.html.twig');
    }

   
    public function loginAction(Request $request)
    {
        dump($request); die();
        return $this->render('PaymentBundle::login.html.twig');
    }
}
